JVectorMap(UK) Files
=====================
jVector map with map data of uk regions


Version 2.0.3
-------------

* jquery-jvectormap-2.0.3.css
* jquery-jvectormap-2.0.3.min.js
* uk-regions-mill.js
